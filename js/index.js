/* highcharts + angularjs + live random data example */

function MyCtrl($scope, limitToFilter) {
    

}

angular.module('myApp', [])
  .directive('hcPie', function() {
    function link(scope, element, attrs) {
          var chart;
          function calculate(){
              var timeStops = []
              
          }
          var chartOptions = {
               chart:{
                    type: 'bar',
                    backgroundColor: '#0C0D19',
                    renderTo:'iotTrack'
               },
               title:{
                    text:''
               },
               colors:['#6699FF','#194578'],
               xAxis: {
                    categories: ['Tracked'],
                    labels:{
                          align:'center',
                          style:{
                               color:'rgba(255, 255, 255, 0.7)',
                               fontSize:'12px',
                               width: '97px'
                          },
                        formatter: function() {
                            return  this.value + '<img  src="./help.svg"  style="vertical-align: middle;width: 14px;height: 14px;margin-left:9px"/>'
                        },
                        useHTML: true
                    },
                    lineColor: '#2c2d39',
                    lineWidth: 1
               },
               yAxis: {
                    type: 'datetime',
                    title:{
                        text:null
                    },
                    labels:{
                       formatter:function(){
                           return moment().startOf('day').format('HH:mm a');
                       },
                       align:'left',
                       style:{
                            color:'rgba(255, 255, 255, 0.7)',
                            fontSize:'12px'
                        }
                    },
                    gridLineColor:'#2c2d39',
                    gridLineWidth:1,
                    opposite:true,
                    reversedStacks : false,
                    crosshair:{
                        snap:false,
                        zIndex:100
                    }    
              },
              tooltip: {
                    crosshairs: [false, true],
                    followPointer:true,
                    backgroundColor : '#FFFFFF',
                    formatter: function () {
                       return '</b> is <b>' + this.y + '</b>';
                    }
              },
              legend: {
                    enabled: false
              },
              plotOptions: {
                    column: {
                        colorByPoint: true
                    },
                    series: {
                        stacking: 'normal',
                        borderWidth:0,
                        pointWidth : 20,
                        cursor: 'pointer'
                    }
             },
             series: [
                    {data:[3]},
                    {data:[1]},
                    {data:[3.3]}
             ],
             credits: {
                  enabled: false
             }
          };

          chart = new Highcharts.chart(chartOptions);
        }  
   
    return {
      restrict: 'E',
      template: '<div id="iotTrack"></div>',
      link: link
    }; 

  });

